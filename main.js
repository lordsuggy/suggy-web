/*
	This main node file is the beginning of everything. EVERYTHING.

	When run, this file will create an express app/server. Then, performers will be instantiated and run. Finally, the express server will listen.

 */

//For hosting the server
const http = require("http");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
//const cookieParser = require("cookie-parser");


//Go ahead and create that server object
const server = http.Server(app);

//This is our handy dandy sutils. This is a hard-coded path which makes us slightly sad, but we'll live
const sutils = require("./shared/sutils.js");

//const Session = require("express-session");
const passport = require("passport");

//This is our logger. 
const Logger = sutils.require("logger.js");


const SuggyEntity = sutils.requireWithSutils("entity/entity.js");

const VERBS = ["post", "patch", "get", "delete", "put"];

const path = require("path");

async function main(){
	await sutils.init();

	//First things first, host our static media
	app.use("/app-static", express.static(path.join(__dirname, "../app/static")));
	app.use("/base-static", express.static(path.join(__dirname, "./base/static")));


	app.use(bodyParser.json());
	//app.use(cookieParser(sutils.config.secret, {secure: true}));

	var DatabaseClass = sutils.requireWithSutils("entity/entityDatabase.js");
	var database = new DatabaseClass();
/*
	const session = Session({
		store: database.getDatastore(Session)
		, secret: sutils.config.secret
		, resave: true
		, saveUninitialized: true
	});

	app.use(session);
	*/

	const performers = await sutils.getPerformers();

	//Do the same for endpoints
	const endpoints = await sutils.getEndpoints();

	
	//We do this after our endpoints set up so a performer can do a catchall if it wants (like the gatekeeper)
	for(let endpoint of endpoints){
		await endpoint.setAppServer(app, server/*, session*/);
	}
	for(let performer of performers){
		await performer.setAppServer(app, server/*, session*/);
	}

	endpoints.forEach(function(endpoint){
		console.log("Looking at " + "/_/"+endpoint.resource);
		var route = app.route("/_/"+endpoint.resource);
		VERBS.forEach(function(verb){
			/*
			const endpointVerbResponse = endpoint[`${verb}Response`];
			if(typeof endpointVerbResponse === "undefined")return;//return is the same as continue in a foreach in javascript
			*/
			console.log(`    Adding verb ${verb}`);
			route = route[verb](
				passport.authenticate('jwt', { session: false }),
				async function(req, res){
				
				console.log("Express User:");
				console.log(req.user);
				const config = sutils.config;//We place this here to ensure that the config is always up to date
				try{ 
					//TODO: get POST variables and use them
					var data = req.body;
					if(verb === "get"){
						data = req.query;
					}
					const originId = data._swoid;
					const originHash = data._swohash;
					delete data._swoid;
					delete data._swohash;

					if(!sutils.originIsValid(originId, originHash)) throw "Origin not verified";
					endpoint.startDurationTimer();

					var user = null;
					if(endpoint.usesUser && typeof req.user !== "undefined" && typeof req.user.id !== "undefined"){
						user = await database.read("user", req.user.id);
						
					}

					const updateProgress = function(data){

						const returnData = {"result":data, "type":"simple"};//await sutils.getReturnData(result, getUser);
						res.write(returnData);
					};


					
					//TODO: turn res into a "user session" object
					const result = await endpoint.process(data, originId, user, updateProgress);
					
					const returnData = await sutils.getReturnData(result, user);
					endpoint.stopDurationTimer();
					returnData.success = true;
					res.send(returnData);
				} catch(err){
					endpoint.stopDurationTimer();
					//Default to just responding with a fail message
					const response = {"success":false};
					//console.log(err);
					if(endpoint.verbose || config.VERBOSE)response["error"] = err;
					res.send(response);
				}
			});
		});
	});




	//Start up the server!
	//TODO: put the port into the config
	server.listen(80, function () {

		//Server has started up
		Logger.log("Main", "Server operational! Listening...");

		//For each of our performers, we'll have them...perform.
		performers.forEach(function(performer){
			//This is how often the performer would like to perform in milliseconds
			/***IMPORTANT*** this is the time in between the end of one performance and the beginning of the next. 
				So, if a performance takes 2 seconds and performPeriod is 1000 (1 seconds), the following will occur:
				T = 0: Performance begins
				T = 2: Performance ends
				T = 3: Performance begins
				T = 5: Performance ends
				T = 6: Performance begins
				T = 8: Performance ends
				...

				This ensures that a single performer is only performing singly or not performing at all at a given time
			*/
			const performPeriod = performer.performPeriod;

			//This is the function they will perform, wrapped in our own
			const performFunction = async function(){ 
				performer.startDurationTimer();
				try {
					//Doing this as an await just in case there are network queries allows us to make sure the task is fully completed
					await performer.perform(); 
				} catch(err){
					Logger.err("Main - Performer: " + performer.name, err);
				}
				performer.stopDurationTimer();

				//If the perform period is 0, only perform once.
				if(performPeriod === 0) return;

				//Otherwise, set this function to occur performPeriod milliseconds from now
				setTimeout(performFunction, performPeriod);
			};

			//We don't call await here because we don't mind starting the performers all at once, as long as each individual one is performing in a singular fashion
			performFunction();

		});
	});
}

main().then(function(){
	console.log("Nexus is running");
}).catch(function(e){
	console.log(e);
});