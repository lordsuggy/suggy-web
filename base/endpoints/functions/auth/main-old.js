/*
    This is our scouter. This code is responsible for:
        - Performing a health check on any existing subsets
*/

var passport = require("passport");
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;


module.exports = function(sutils){

	var DatabaseClass = sutils.require("entity/entityDatabase.js")(sutils);
	var database = new DatabaseClass();

	var EntityEndpoint = sutils.require("endpoints/entityEndpoint.js")(sutils);
	

	const FunctionEndpoint = sutils.require("endpoints/functionEndpoint.js")(sutils);

	class AuthEndpoint extends FunctionEndpoint {

		constructor() {
			super();
		}

		async setAppServer(app, server){
			const credentials = await sutils.readCredentials("gcp");
			const config = sutils.config;

			app.use(passport.initialize());
			app.use(passport.session());

			var callbackURL = "";
			callbackURL += config.secure ? "https://" : "http://";
			callbackURL += config.domain;
			callbackURL += "/auth/google/callback";

			passport.use(new GoogleStrategy({
				clientID: credentials.client_id,
				clientSecret: credentials.client_secret,
				callbackURL: callbackURL
			},

			async function(accessToken, refreshToken, profile, done) {
				console.log("Setting user credentials");
				const filters = [{"key": "googleId", "value":profile.id}];
				var user;
				var userInfo;
				try{
					user = await database.querySingle("sw-user", filters);
					userInfo = await user.info;
				} catch(e){
					if(e === "Found no results"){
						const identifier = sutils.uuid();
						const userId = sutils.uuid();
						const permissions = {};
						permissions["user:"+userId] = DatabaseClass.PERMISSIONS.READ;
						user = await database.create("sw-user", identifier, {"googleId":profile.id});
						userInfo = await database.create("user", userId, {_p:permissions});
						user.info = userInfo;
						await user.save();

					}else{
						console.log(e);
						return done(e);
					}
				}

				return done(null, userInfo._identifier);

				/*
				User.findOrCreate({ googleId: profile.id }, function (err, user) {
					return done(err, user);
				});
				}
				*/
			}));


			// used to serialize the user for the session
			passport.serializeUser(function(userId, done) {
				console.log("-----Serializing user-----");
				console.log(userId);
				done(null, userId);
			});

			// used to deserialize the user
			passport.deserializeUser(function(id, done) {
				console.log("-----Deserializing user-----");
				console.log(id);
				var err = null;
				done(err, id);
				/*
				database.read("user", id).then(function(userInfo){
					console.log("User found!");
					done(err, userInfo);
				}).catch(function(err){
					done(err);
				});*/

			});

			console.log("Setting /auth/google endpoint");
			// GET /auth/google
			//   Use passport.authenticate() as route middleware to authenticate the
			//   request.  The first step in Google authentication will involve
			//   redirecting the user to google.com.  After authorization, Google
			//   will redirect the user back to this application at /auth/google/callback
			app.get("/auth/google"
				, function(req, res, next){
					var scopes;
					try{
						scopes = JSON.parse(req.query.scopes);
					}catch(e){

					}
					if(typeof scopes === "undefined")scopes = [];
					var targetUrl = req.query.targetUrl
					if(typeof targetUrl === "undefined")targetUrl = "/";
					scopes.push("https://www.googleapis.com/auth/plus.login");
					return passport.authenticate("google", { scope: scopes, state: targetUrl })(req, res, next);
				});//TODO: set scopes from request url

			// GET /auth/google/callback
			//   Use passport.authenticate() as route middleware to authenticate the
			//   request.  If authentication fails, the user will be redirected back to the
			//   login page.  Otherwise, the primary route function function will be called,
			//   which, in this example, will redirect the user to the home page.
			app.get("/auth/google/callback",
				passport.authenticate("google", { failureRedirect: "/" }),
				function(req, res) {
					req.session.save(() => {
				      res.redirect(req.query.state);
				    })
				});

			app.get('/logout', function(req, res){
				req.logout();
				console.log("LOGGING OUT! :)")

				var targetURL = req.query.targetURL
				if(typeof targetURL === "undefined")targetURL = "/";
				res.redirect(targetURL);
			});
		}

		async ioFunction(inputs, user){
			const action = inputs.action;
		}
	}

	return AuthEndpoint;

};