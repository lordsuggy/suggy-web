/*
    This is our scouter. This code is responsible for:
        - Performing a health check on any existing subsets
*/

var passport = require("passport");
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;

module.exports = function(sutils){

	const AUTH_RESOURCE = "/auth/google";
	const CALLBACK_RESOURCE = AUTH_RESOURCE+"/callback";
	class GoogleAuthProvider {

		constructor() {

		}

		async setupPassport( callbackUrlBase, getUserIdFromEmail){
			
			const credentials = await sutils.readCredentials("gcp");

			//Set up passport
			passport.use(new GoogleStrategy({
				clientID: credentials["client_id"]
				, clientSecret: credentials["client_secret"]
				, callbackURL: callbackUrlBase+CALLBACK_RESOURCE
			},

			async function(accessToken, refreshToken, profile, done) {
				try{
					console.log("Setting user credentials");
					const initialData = {};
					initialData.googleId = profile.id;
					const tokens = [
					{
						"provider":"google"
						, "access": accessToken
						, "refresh": refreshToken
					}];
					await getUserIdFromEmail(profile.emails[0].value, initialData, tokens, done);
				}catch(e){
					console.log(e);
				}
			}));


		}

		setupEndpoints(app, requestAuthenticated){

			console.log("Setting /auth/google endpoint");
			// GET /auth/google
			//   Use passport.authenticate() as route middleware to authenticate the
			//   request.  The first step in Google authentication will involve
			//   redirecting the user to google.com.  After authorization, Google
			//   will redirect the user back to this application at /auth/google/callback
			app.get(AUTH_RESOURCE
				, function(req, res, next){
					var scopes;
					try{
						scopes = JSON.parse(req.query.scopes);
					}catch(e){

					}
					var targetUrl = req.query.targetUrl
					if(typeof targetUrl === "undefined")targetUrl = "/";

					if(typeof scopes === "undefined")scopes = [];
					scopes.push("profile");
					scopes.push("email");
					return passport.authenticate("google", { scope: scopes, state: targetUrl, session:false })(req, res, next);
				});//TODO: set scopes from request url

			// GET /auth/google/callback
			//   Use passport.authenticate() as route middleware to authenticate the
			//   request.  If authentication fails, the user will be redirected back to the
			//   login page.  Otherwise, the primary route function function will be called,
			//   which, in this example, will redirect the user to the home page.
			app.get(CALLBACK_RESOURCE,
				passport.authenticate("google", { failureRedirect: "/", session:false }),
				function(req, res) {
					
				    requestAuthenticated(req, res, req.query.state);
				});

		}

	}

	return GoogleAuthProvider;

};