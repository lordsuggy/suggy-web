/*
    This is our balancer script. This code is responsible for:
        - Spinning up other similar instances, if necessary
        - Spinning down other similar instances, if necessary
*/

module.exports = function(sutils) { //This allows us to not have to worry about hard-coding paths, as well as providing this handy utils object

	const Performer = sutils.require("performer.js");
	const Logger = sutils.require("logger.js");

	class CartographerPerformer extends Performer {

		constructor(){
			super();
		}

		async getGraphInfo(){
			const performers = await sutils.getPerformers();
			const endpoints = await sutils.getEndpoints();
			const config = sutils.config;
			return {
				"id": config.id
				, "address": config.address
				, "parent": config.parent
				, "secure": config.secure
				, "performers": performers.map(function(performer){
					return performer.name;
				})
				, "endpoints": endpoints.map(function(endpoint){
					return endpoint.resource;
				})
			};
		}

		async perform(){
			Logger.log("CartographerPerformer", "Performing");
			const config = sutils.config;

			var graph;
			//If the graph doesn't exist, create it. TODO: Yeah we're recreating it no matter what right now. Woohooo
			//
			//TODO: In the future, we'll take care of subgraphs and the like
			//For now, we'll just handle the single server use case
			
			//If we're the root, it's the easiest case
			if(config.parent === -1){
				//See if we're a messenger
				const messengers = [];
				if(config.base.performers.length === 0 || config.base.performers.indexOf("Messenger") !== -1){
					messengers.push(config.id);
				}
				graph = {
					servers: [
						await this.getGraphInfo()
					]
					, messengers: messengers
				};
			}
			await sutils.writeJsonToFile("graph", graph);
			
		}
	}

	return CartographerPerformer;
};
