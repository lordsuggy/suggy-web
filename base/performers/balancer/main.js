/*
    This is our balancer script. This code is responsible for:
        - Spinning up other similar instances, if necessary
        - Spinning down other similar instances, if necessary
*/

module.exports = function(sutils) { //This allows us to not have to worry about hard-coding paths, as well as providing this handy utils object

	const Performer = sutils.require("performer.js");
	const Logger = sutils.require("logger.js");

	class BalancePerformer extends Performer {

		constructor(){
			super();
		}

		async perform(){
			Logger.log("BalancerPerformer", "Performing");
		}
	}

	return BalancePerformer;
};
