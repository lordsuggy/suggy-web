/*
    This is our reporter. This code is responsible for:
        - Performing a health check on ourselves
*/

module.exports = function(sutils){

	const Performer = sutils.require("performer.js");
	const Logger = sutils.require("logger.js");

	class ReporterPerformer extends Performer {

		constructor() {
			super();

			//In our convention, this is a publicly accessible field
			//this.performPeriod = 5000;//This is the number of milliseconds in between each call to "perform". 0 signifies that it's only called once.

		}

		async perform(){
			Logger.log("ReporterPerformer", "Performing");

			//const performers = await sutils.getPerformers();
			//const endpoints = await sutils.getEndpoints();

			//TODO: use the duration times from performers and endpoints to determine load
			const config = sutils.config;

			/*
			Config looks something like 
			{
				"app": {
					"performers": []
					, "endpoints": []
				}
				, "base": {
					"performers": []
					, "endpoints": []
				}
				, "parent": "-1"
				, "id": "0"
			}

			 */
			
			//We want to generate something like 
			/*
			{
				
				"performers": {"performerName":load%}
				, "endpoints": {}
				, "parent": "-1"
				, "id": "0"
			}

			 */
			
			const state = {};
			state.performers = {};
			state.endpoints = {};

			function reportStates(operations, toList){
				operations.forEach(function(operation){
					toList[operation.name] = operation.durationPerformed;
					operation.resetDurationPerformed();
				});
			}
			

			reportStates(await sutils.getPerformers(),  state.performers);
			reportStates(await sutils.getEndpoints(), state.endpoints);

			state.parent = config.parent;
			state.id = config.id;

			await sutils.writeJsonToFile("state", state);

		}
	}

	return ReporterPerformer;

};