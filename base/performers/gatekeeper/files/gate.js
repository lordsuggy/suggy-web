SuggyUtils = {
	clearElement: function(element) {
		while (element.firstChild) {
		    element.removeChild(element.firstChild);
		}
	}
	, addLib: async function(head, lib, source){
		return new Promise(function(resolve){
			const element = document.createElement("script");
			element.src = `/${source}-static/libs/`+lib;
			element.async = false;
			element.addEventListener("load", function() {
				resolve();
			});
			head.appendChild(element);
		});
	}
	, getQuery() {
	    const dictionary = {};
	    var queryString = window.location.search;
	     
	    // remove the '?' from the beginning of the
	    // if it exists
	    if (queryString.indexOf('?') === 0) {
	        queryString = queryString.substr(1);
	    }
	     
	    // Step 1: separate out each key/value pair
	    const parts = queryString.split('&');
	     
	    for(var i = 0; i < parts.length; i++) {
	        const p = parts[i];
	        // Step 2: Split Key/Value pair
	        const keyValuePair = p.split('=');
	         
	        // Step 3: Add Key/Value pair to Dictionary object
	        const key = keyValuePair[0];
	        var value = keyValuePair[1];
	         
	        // decode URI encoded string
	        value = decodeURIComponent(value);
	        value = value.replace(/\+/g, ' ');
	         
	        dictionary[key] = value;
	    }
	     
	    // Step 4: Return Dictionary Object
	    return dictionary;
	}
	, diffLists: function(oldList, newList, areEqual){
		const results = {};
		results.removed = this._diffRemoved(oldList, newList, areEqual);
		results.added = this._diffRemoved(newList, oldList, areEqual);
		const untouched = [];
		for(let element of oldList){
			if(results.removed.indexOf(element) !== -1)continue;
			untouched.push(element);
		}
		results.untouched = untouched;
		return results;
	}
	, _diffRemoved: function(oldList, newList, areEqual){
		const removed = [];
		for(let oldElement of oldList) {
			var found = false;
			for(let newElement of newList) {
				if(typeof areEqual === "undefined"){
					found = oldElement === newElement;
				}else{
					found = areEqual(oldElement, newElement);
				}
				if(found)break;
			}
			if(!found){
				//It was in the old but not the new, so it has been removed
				removed.push(oldElement);
			}
		}
		return removed;
	}
}

class SuggyWeb {
	constructor(){
		const thiz = this;
		this._baseResource = window.location.pathname.split("/")[1];
		this._entityCache = {};
		this._kindInfos = {};
		this._templates = {};
		this._ws = new SuggyIO(this);
		this.PROVIDERS = Object.freeze({"GCP":"GCP"});//Kind of hackish, but this refers to the same providers in /shared/providers/provider
		/*
		this.UNSAFE_CHARS = [];//["<", ">", "&", "\"", "'", "`", " ", "!", "@", "$", "%", "(", ")", "=", "+", "{", "}", "[", "]"];
		this.UNSAFE_CHAR_REPLACEMENTS = [];

		RegExp.escape= function(s) {
		    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
		};

		//NOTE! Be extremely careful if you ever choose to add "#", ";", or any number to the list

		//Ampersand must come first
		this._addUnsafeChar("&", "&#38;");
		this._addUnsafeChar("<", "&#60;");
		this._addUnsafeChar(">", "&#62;");
		this._addUnsafeChar("\"", "&#34;");
		this._addUnsafeChar("'", "&#39;");
		this._addUnsafeChar("`", "&#96;");
		this._addUnsafeChar(" ", "&#32;");
		this._addUnsafeChar("!", "&#33;");
		this._addUnsafeChar("@", "&#64;");
		this._addUnsafeChar("$", "&#36;");
		this._addUnsafeChar("%", "&#37;");
		this._addUnsafeChar("(", "&#40;");
		this._addUnsafeChar(")", "&#41;");
		this._addUnsafeChar("=", "&#61;");
		this._addUnsafeChar("+", "&#43;");
		this._addUnsafeChar("{", "&#123;");
		this._addUnsafeChar("}", "&#125;");
		this._addUnsafeChar("[", "&#91;");
		this._addUnsafeChar("]", "&#93;");
		*/

		this._ws.setMessageHandler(function(msg){thiz.handleMessage(msg);});
	}

	//Entity is an entity we can pass into the page as an argument
	loadResource(resource, entity){//TODO: add SEO resource path items like entity name
		window.location.href = "/"+resource + "?e="+entity._kind + ":"+entity._identifier;
	}

	//This is a dangerous method as it uses innerHTML -- try to prevent other developers from using it as much as possible
	__appendElement(parent, elementType, elementString){
		const element = document.createElement(elementType);
		element.innerHTML = elementString;
		parent.appendChild(element);
	}

	_handleEntityMessage(msg){
		const action = msg.action;
		const data = msg.data;
		const kindIdentifier = this._getKindIdentifier(data.kind, data.id);
		const entity = this._entityCache[kindIdentifier];
		console.log("Entity cache:");
		console.log(this._entityCache);
		console.log(entity);
		if(typeof entity === "undefined")return;
		if(action === "delete"){
			entity.setDeleted();
		}else if(action === "update"){
			const fields = data.data;
			for(var fieldName in fields){
				entity.updateField(fieldName, fields[fieldName]);//TODO: perhaps do a more precise check for changes in dictionaries/arrays
			}
		}

	}

	handleMessage(msg){
		console.log(msg);
		const type = msg.type;
		if(type === "entity"){
			this._handleEntityMessage(msg);
		}
	}
	/*
	_addUnsafeChar(char, replacement){
		this.UNSAFE_CHARS.push(char);
		this.UNSAFE_CHAR_REPLACEMENTS.push(replacement);
	}
	*/

	async getTemplate(templateName){
		var template = this._templates[templateName];
		if(typeof template !== "undefined")return template;
		template = await this._callFull("w/"+ this._baseResource, "t", {"template":templateName});
		const AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;
		template.func = new AsyncFunction("entity", "element", "SWeb", template.js);
		this._templates = template;
		//Load up CSS once (it'll be cached in the future, so we won't reload it)
		if(template.css !== ""){
			const cssElement = document.createElement("style");
			cssElement.innerHTML = template.css;
			document.getElementsByTagName("head")[0].appendChild(cssElement);
		}
		return template;
	}


	async call(functionName, inputs, updateCallback){
		//TODO: possibly add a queue and also check on the ws's status to store and forward, if necessary
		return await this._callFull("f/"+functionName, "f", inputs, updateCallback);
	}

	async _callFull(endpoint, action, inputs, updateCallback){
		//TODO: possibly add a queue and also check on the ws's status to store and forward, if necessary
		const data = {};
		data.inputs = inputs;
		data.action = action;
		data.endpoint = endpoint;
		return await this._ws.request(data, updateCallback);
	}

	login(provider, scopes, targetUrl){
		const scopesJson = typeof scopes === "undefined" ? "[]" : JSON.stringify(scopes);
		const scopesJsonEncoded = encodeURIComponent(scopesJson);
		if(typeof targetUrl === "undefined")targetUrl = encodeURIComponent(window.location.pathname);
		//We aren't using template literals in case the browser doesn't support it.
		window.location = "auth/"+provider+"?scopes="+scopesJsonEncoded+"&targetUrl="+targetUrl;
	}

	async getUser(){
		if(typeof this._user !== "undefined")return this._user;
		const user = await this.call("get-user");
		if(user !== null){
			console.log("Setting cache: ");
			console.log(user);
			this._entityCache[this._getKindIdentifier(user._kind, user._identifier)] = user;
			this._user = user;
		}
		return user;
	}

	async getArg(){
		const query = SuggyUtils.getQuery();
		const entityKey = query.e;

		var arg = this._entityCache[entityKey];
		if(typeof arg !== "undefined")return arg;

		const entityKeySplit = entityKey.split(":");
		const entityKind = entityKeySplit[0];
		const entityId = entityKeySplit[1];

		arg = await this.entity.get(entityKind, entityId);
		if(arg !== null){
			//await this.entity.subscribe(arg);
			this._entityCache[entityKey] = arg;
		}
		return arg;
	}
	/*
	sanitize(string){
		const thiz = this;
		this.UNSAFE_CHARS.forEach(function(unsafeChar, index){
			const replacementChar = thiz.UNSAFE_CHAR_REPLACEMENTS[index];
			string = string.replace(new RegExp(RegExp.escape(replacementChar), 'g'), unsafeChar);
			string = string.replace(new RegExp(RegExp.escape(unsafeChar), 'g'), replacementChar);
		});
		
		return string;
	}
	*/

	async _getKindInfo(kind){
		try{
			var kindInfo = this._kindInfos[kind];
			if(typeof kindInfo === "undefined"){
				kindInfo = await this._callFull("e/"+kind, "k");
				this._kindInfos[kind] = kindInfo;
			}
			return kindInfo;
		}catch(e){
			console.log("Error ");
			console.log(e);
			return null;
		}
	}

	_getKindIdentifier(kind, identifier){
		return kind + ":" + identifier;
	}


	get entity(){
		const thiz = this;
		return {
			/*
			create: async function(kind, data){
				const entityData = await thiz._callFull("e/"+kind, "c", {"data":data});
				const newEntity = await thiz._getSuggyEntity(entityData, kind);
				thiz._entityCache[thiz._getKindIdentifier(kind, newEntity._identifier)] = newEntity;
				return newEntity;
			}
			, */
			read: async function(kind, identifier, customFunction, customArgs){


				const kindIdentifier = thiz._getKindIdentifier(kind, identifier);
				const existingEntity = thiz._entityCache[kindIdentifier];
				if(typeof existingEntity !== "undefined" && existingEntity._deleted)return null;//Return null if the entity is deleted
				if(typeof existingEntity !== "undefined")return existingEntity;

				var readFunction = "e/"+kind;
				var readAction = "r";
				if(typeof customFunction !== "undefined"){
					if(typeof customArgs === "undefined"){
						customArgs = {};
					}
					customArgs.id = identifier;
					customArgs.kind = kind;
					readFunction = "f/"+customFunction;
					readAction = "f";
				}
				const readRequest = {"subscribe":true, "id":identifier};
				if(typeof customArgs !== "undefined"){
					readRequest.args = customArgs;
				} 
				const newEntity = await thiz._callFull(readFunction, readAction, readRequest);

				thiz._entityCache[kindIdentifier] = newEntity;

				newEntity.setCustomReadFunctionAndArgs(customFunction, customArgs);

//				await thiz.entity.subscribe(newEntity);
				return newEntity;
			}
			, get: async function(kind, identifier, customFunction, customArgs){
				return await this.read(kind, identifier, customFunction, customArgs);
			}
			/*
			, update: async function(kind, identifier, data){
				const entityData = await thiz._callFull("e/"+kind, "u", {"id":identifier, "data":data});
				return await thiz._getSuggyEntity(entityData, kind);
			}
			, delete: async function(kind, identifier){
				//TODO: we should delete from cache here, or mark the entity as deleted
				await thiz._callFull("e/"+kind, "d", {"id":identifier});
			}
			*/
			, subscribe: async function(entity){
				await thiz._callFull("e/"+entity._kind, "s", {"id":entity._identifier});

			}
			, unsubscribe: async function(entity){
				await thiz._callFull("e/"+entity._kind, "b", {"id":entity._identifier});

			}
		}
	}

}


class SuggyIO {
	constructor(sweb){

		this._pageLoaded = false;
		this._sweb = sweb;
		this._reconnectTime = 500;
		this.connect();
		this._msgHandler;
		//this._entities = {};
	}

	setMessageHandler(handler){
		this._msgHandler = handler;
	}

	async getResult(jsonData){
		const thiz = this;
		var result = jsonData.result;
		if(jsonData.type === "entity"){
			const kindInfo = await thiz._sweb._getKindInfo(result.kind);
			const entityKey = result.kind + ":" + result.data._id;
			var existingEntity = thiz._sweb._entityCache[entityKey];
			if(typeof existingEntity === "undefined"){
				console.log("Creating new entity");
				existingEntity = new SuggyEntity(kindInfo, result.data, thiz._sweb);
				console.log(existingEntity);
				thiz._sweb._entityCache[entityKey] = existingEntity
			} else {
				console.log("Updating existing entity");
				existingEntity.updateWithData(result.data);
			}

			result = existingEntity;
		}
		return result;
	}

	async connect() {
		const thiz = this;
		const sweb = this._sweb;
		//TODO: We should choose the protocol based on the server's security
		const socket = io("http://${messengerAddress}/", {
			transports: ["websocket"]
		});

		socket.on("swupd", async function(update){
			const jsonData = update;
			console.log("Update:");
			console.log(jsonData);
			
			//TODO: right now we only do request/response; in the future, we'll add listeners
			const requestId = jsonData.id;
			const requestPromise = thiz._requests[requestId];
			if(typeof requestPromise === "undefined")return;


			const result = await thiz.getResult(jsonData);
			requestPromise.update(result);
		});
		
		socket.on("swres", async function (result) {
			const jsonData = result;
			console.log("Result:");
			console.log(jsonData);
			
			//TODO: right now we only do request/response; in the future, we'll add listeners
			const requestId = jsonData.id;
			const requestPromise = thiz._requests[requestId];
			if(typeof requestPromise === "undefined")return;

			delete thiz._requests[requestId];

			if(!jsonData.success){
				requestPromise.reject(jsonData.error);
				return;
			}
			var result = await thiz.getResult(jsonData);
			requestPromise.resolve(result);
		});

		socket.on("swmsg", function(msg){

			console.log("Msg:");
			console.log(msg);
			if(typeof thiz._msgHandler === "undefined") return;
			thiz._msgHandler(msg);
		});

		socket.on("swrdy", async function(){
			if(thiz._pageLoaded)return;
			await thiz.loadPage();
		});

		socket.on("swlogout", async function(){
			localStorage.removeItem("swt");
		});
		

		socket.on("connect", async function () {

			const swt = window.localStorage.getItem("swt");
			const tokenData = {};
			if(typeof swt !== "undefined"){
				tokenData.swt = swt;
			}
			socket.emit("swauth", tokenData);
		});
		
		/*
		ws.onclose = async function () {
			console.log("WS Closed -- attempting to reconnect");
			setTimeout(async function(){
				await thiz.connect();
			}, this._reconnectTime);
		};

		ws.onerror = async function(error) {
			console.log("WS Errored");
			console.log(error);
		};
		*/
	
		this._socket = socket;

		this._requests = {};
		this._requestId = 0;
	}

	async loadPage()	{
		const sweb = this._sweb;
		const endpoint = window.location.pathname.substr(1);//Remove the initial slash
		const indexRequest = {"action":"w", "endpoint":"w/"+endpoint};
		const indexResponse = await this.request(indexRequest);
		this._pageLoaded = true;

		const js = indexResponse.js;
		const css = indexResponse.css;
		const body = indexResponse.body;
		const appLibs = indexResponse.alibs;

		var script = `function sweb_main(SWeb){${js}}`;

		const head = document.head || document.getElementsByTagName("head")[0];
		sweb.__appendElement(head, "style", css);
//		sweb.appendElement(document.getElementById("sweb-body"), "span", body);
		document.getElementById("sweb-body").innerHTML = body;

		for(let lib of appLibs){
			await SuggyUtils.addLib(head, lib, "app");
		}

		sweb.__appendElement(document.getElementById("sweb-scripts"), "script", script);

		sweb_main(sweb);
	}

	async request(data, updateCallback){
		const thiz = this;
		return new Promise(function(resolve, reject){
			data.id = thiz._requestId;
			//TODO: Add a timeout so we clean up this entity after some time
			thiz._requests[thiz._requestId] = {"resolve":resolve, "reject":reject, "update":updateCallback};

			thiz._requestId += 1;
			thiz._socket.emit("swreq", data);
			console.log("Requesting ");
			console.log(data);
			//thiz._ws.send(JSON.stringify(data));
		});
	}
}

//Developer should be able to call await entity.fieldName to get the value
class SuggyEntity {
	constructor(kindInfo, entityData, sweb){
		this._kindInfo = kindInfo;
		this._data = entityData;
		this._convertedData = {};
		this._sweb = sweb;
		this._fieldListeners = {};
		this._kind = kindInfo.name;
		this._identifier = entityData._id;
		this._boundElements = [];
		this._deleted = false;
		this._swLoadedElements = [];
		this.init(kindInfo);


	}

	setCustomReadFunctionAndArgs(customFunction, customArgs){
		this._customReadFunction = customFunction;
		this._customReadArgs = customArgs;
	}

	updateWithData(data){
		this._data = data;
	}

	init(kindInfo) {
		const thiz = this;
		//Go through the fields and create getters for them as appropriate
		for(let field of kindInfo.fields) {
			const fieldName = field.name;
			const fieldType = field.type;
			const fieldDefault = field.default;
			thiz._fieldListeners[fieldName] = [];//TODO: if we end up being able to add fields dynamically (after page load), then we'll want to add the field to the field listener as well
			if(fieldType === "boolean" || fieldType === "number" || fieldType === "string"){
				thiz.setSimpleField(fieldName, fieldType, fieldDefault);
			}else if(fieldType === "array"){
				thiz.setArrayField(fieldName);
			}else if(fieldType === "dictionary"){
				thiz.setDictionaryField(fieldName);
			}else if(fieldType === "entity"){
				thiz.setEntityField(fieldName);
			}else if(fieldType === "entity-array"){
				thiz.setEntityArrayField(fieldName);
			}else if(fieldType === "entity-dictionary"){
				thiz.setEntityDictionaryField(fieldName);
			}
		}
	}

	setSimpleField(fieldName, fieldType, defaultValue) {
		const thiz = this;
		Object.defineProperty(thiz, fieldName, {
			get: async function () {
				const currentValue = thiz._data[fieldName];
				return typeof currentValue === "undefined" ? defaultValue : currentValue;
			}
			/*
			, set: function(val) {
				if(typeof val === fieldType){
					thiz._data[fieldName] = val;
				}
			}
			*/
		});
	}

	setArrayField(fieldName) {
		const thiz = this;
		Object.defineProperty(thiz, fieldName, {
			get: async function () {
				var currentValue = thiz._data[fieldName];
				if(typeof currentValue === "undefined"){
					currentValue = [];
					thiz._data[fieldName] = currentValue;
				}
				return Object.freeze(currentValue);
			}
			/*
			, set: function(val) {
				if(Array.isArray(val)){
					thiz._data[fieldName] = val;
				}
			}
			*/
		});
	}

	setDictionaryField(fieldName) {
		const thiz = this;
		Object.defineProperty(thiz, fieldName, {
			get: async function () {
				var currentValue = thiz._data[fieldName];
				if(typeof currentValue === "undefined"){
					currentValue = {};
					thiz._data[fieldName] = currentValue;
				}
				return Object.freeze(currentValue);
			}
			/*
			, set: function(val) {
				if(typeof val === "object" && !Array.isArray(val)){
					thiz._data[fieldName] = val;
				}
			}*/
		});
	}

	setEntityField(fieldName) {
		const thiz = this;
		Object.defineProperty(thiz, fieldName, {
			get: async function () {
				var currentValue = thiz._convertedData[fieldName];
				//If it isn't undefined, we've already got the values
				if(typeof currentValue !== "undefined")return currentValue;

				//If it is undefined, we need to get the values
				currentValue = thiz._data[fieldName];
				
				//If the currentValue is undefined in the raw data, then return undefined
				if(typeof currentValue === "undefined")return currentValue;

				//If the currentValue is defined in the raw data, pull it down
				const convertedValue = await thiz.getEntityFromValue(currentValue);
				thiz._convertedData[fieldName] = convertedValue;
				return convertedValue;

			}
			/*
			, set: function(val) {
				if(!(val instanceof SuggyEntity))return;
				thiz._data[fieldName] = thiz.getValueFromEntity(val);
				thiz._convertedData[fieldName] = val;
			}*/

		});
	}

	setEntityArrayField(fieldName) {
		const thiz = this;
		Object.defineProperty(thiz, fieldName, {
			get: async function () {
				var currentValue = thiz._convertedData[fieldName];
				//If it isn't undefined, we've already got the values
				if(typeof currentValue !== "undefined")return Object.freeze(currentValue);

				//If it is undefined, we need to get the values
				currentValue = thiz._data[fieldName];
				
				//If the currentValue is undefined in the raw data, then return an empty array
				if(typeof currentValue === "undefined"){
					currentValue = [];
					this._convertedData[fieldName] = currentValue;
					return Object.freeze(currentValue);
				}

				//If the currentValue is defined in the raw data, pull it down
				const currentValuePromises = currentValue.map(function(val){
					return thiz.getEntityFromValue(val);
				});

				const convertedValue = await Promise.all(currentValuePromises);
				thiz._convertedData[fieldName] = convertedValue;
				return Object.freeze(convertedValue);
			}
			/*
			, set: function(vals) {

				if(!Array.isArray(vals))return;
				const unconvertedVals = vals.filter(function(val){
					return (val instanceof SuggyEntity);
				}).map(function(val){
					return thiz.getValueFromEntity(val);
				});

				thiz._data[fieldName] = unconvertedVals;
				thiz._convertedData[fieldName] = vals;
			}
			*/
		});
	}

	setEntityDictionaryField(fieldName) {
		const thiz = this;
		Object.defineProperty(thiz, fieldName, {
			get: async function () {
				var currentValue = thiz._convertedData[fieldName];
				//If it isn't undefined, we've already got the values
				if(typeof currentValue !== "undefined")return Object.freeze(currentValue);

				//If it is undefined, we need to get the values
				currentValue = thiz._data[fieldName];
				
				//If the currentValue is undefined in the raw data, then return an empty array
				if(typeof currentValue === "undefined"){
					currentValue = {};
					this._convertedData[fieldName] = currentValue;
					return Object.freeze(currentValue);
				}

				//If the currentValue is defined in the raw data, pull it down
				
				currentValue = {};
				const currentValuePromises = Object.keys(currentValue).map(function(key){
					return thiz.getEntityFromValue(val).then(function(entity){
						currentValue[key] = entity;
					});
				});

				await Promise.all(currentValuePromises);
				thiz._convertedData[fieldName] = currentValue;
				return Object.freeze(currentValue);
			}
			/*
			, set: function(vals) {
				if(typeof vals !== "object" || Array.isArray(vals))return;

				const unconvertedVals = Object.keys(vals).filter(function(key){
					const val = vals[key];
					return (val instanceof SuggyEntity);
				}).map(function(key){
					const val = vals[key];
					return thiz.getValueFromEntity(val);
				});

				thiz._data[fieldName] = unconvertedVals;
				thiz._convertedData[fieldName] = vals;
			}
			*/
		});
	}

	//For the next two functions, we assume kinds and identifiers don't use colons (:)
	async getEntityFromValue(value){
		const valueSplit = value.split(":");
		const kind = valueSplit[0];
		const identifier = valueSplit[1];

		//TODO: should we cache this value if it's being synced?  -- Now being done
		return await this._sweb.entity.get(kind, identifier, this._customReadFunction, this._customReadArgs);
	}

	getValueFromEntity(entity){
		return entity._kind + ":" + entity._identifier;
	}

	async getPermissionRole(roles){
		const thiz = this;
		const entities = [];
		const permissions = this._data._p;
		for(const entityKey in permissions){
			const permission = permissions[entityKey];
			if(roles.indexOf(permission) === -1)continue;
			const entity = await thiz.getEntityFromValue(entityKey);
			entities.push(entity);
		}
		return entities;
	}

	async getReaders(){
		const thiz = this;
		const entities = [];
		const permissions = this._data._p;
		for(const entityKey in permissions){
			if(entityKey === "*:*")continue;//TODO: we should return "global" or "public" 

			const permission = permissions[entityKey];
			if(permission !== "r")continue;
			const entity = await thiz.getEntityFromValue(entityKey);
			entities.push(entity);
		}
		return entities;
	}

	_getFieldInfo(fieldName){
		const fields = this._kindInfo.fields;
		for(let fieldInfo of fields){
			if(fieldInfo.name === fieldName) return fieldInfo;
		}
		return null;
	}

	bindSimpleField(field, element) {
		const thiz = this;
		this._boundElements.push(element);
		async function updateElement(){
			if(!document.contains(element) || thiz._boundElements.indexOf(element) === -1){
				//Remove the listener
				thiz.unlistenToField(field, this);
				return;
			}
			element.textContent = await thiz[field];
		}
		this.listenToField(field, updateElement);
		updateElement();
	}

	bindEntityField(field, element) {
		const thiz = this;
		this._boundElements.push(element);
		async function updateElement(){
			if(!document.contains(element) || thiz._boundElements.indexOf(element) === -1){
				//Remove the listener
				thiz.unlistenToField(field, this);
				return;
			}

			const fieldEntity = await thiz[field];
			fieldEntity.bindTo(element);
		}
		this.listenToField(field, updateElement);
		updateElement();
	}

	bindEntityArrayField(field, element) {
		const thiz = this;
		this._boundElements.push(element);
		async function updateElement(){
			if(!document.contains(element) || thiz._boundElements.indexOf(element) === -1){
				//Otherwise, remove the listener
				thiz.unlistenToField(field, this);
				return;
			}
			const fieldEntities = await thiz[field];
			SuggyUtils.clearElement(element);

			for(let fieldEntity of fieldEntities){
				const fieldEntityKind = fieldEntity._kind;
				const template = await thiz._sweb.getTemplate(fieldEntityKind);
				const newElement = document.createElement("span");
				newElement.innerHTML = template.html;
				element.appendChild(newElement);
				await template.func(fieldEntity, newElement, thiz._sweb);
				fieldEntity.bindTo(newElement);
			}
			
		}
		this.listenToField(field, updateElement);
		updateElement();
	}

	bindTo(element) {
		const children = element.children; 
		for(let child of children){
			const field = child.dataset.swField;
			//If the field is unspecified, bind further (depth-first)
			if(typeof field === "undefined"){
				this.bindTo(child);
				continue;
			}

			//If the field is specified, we'll set that field based on what type it is.
			
			const fieldInfo = this._getFieldInfo(field);
			const fieldType = fieldInfo.type;
			if(fieldType === "boolean" || fieldType === "string" || fieldType === "number"){
				this.bindSimpleField(field, child);
			}else if(fieldType === "entity"){
				this.bindEntityField(field, child);
			}else if(fieldType === "entity-array"){
				this.bindEntityArrayField(field, child);
			}
		}
	}

	unbind(){
		this._boundElements.length = 0;
	}

	unbindFrom(element){
		const elementIndex = this._boundElements.indexOf(element);
		if(elementIndex !== -1){
			this._boundElements.splice(elementIndex, 1);
		}

		const children = element.children; 
		for(let child of children){
			this.unbindFrom(child);
		}
	}

	async loadInto(element) {
		//This will load the template and stick it into the element
		const template = await this._sweb.getTemplate(this._kind);
		const container = document.createElement("span");
		container.innerHTML = template.html;

		SuggyUtils.clearElement(element);
		element.appendChild(container);
		await template.func(this, container, this._sweb);

		this.bindTo(container);

		this._swLoadedElements.push(element);
		const previouslyLoadedEntity = element._swLoadedEntity;
		if(typeof previouslyLoadedEntity !== "undefined"){
			const previousElements = previouslyLoadedEntity._swLoadedElements;
			const previousEntityIndex = previousElements.indexOf(previouslyLoadedEntity);
			if(previousEntityIndex !== -1){
				previousElements.splice(previousEntityIndex, 1);
			}
		}
		element._swLoadedEntity = this;
		console.log("Loaded element");
		console.log(this._swLoadedElements);

	}

	listenToField(fieldName, listenerCallback){
		const fieldListeners = this._fieldListeners[fieldName];
		if(typeof fieldListeners === "undefined")return;
		fieldListeners.push(listenerCallback);
	}

	unlistenToField(fieldName, listenerCallback){
		const fieldListeners = this._fieldListeners[fieldName];
		if(typeof fieldListeners === "undefined")return;
		const index = fieldListeners.indexOf(listenerCallback);
		if(index === -1)return;
		fieldListeners.split(index, 1);
	}

	updateField(field, value){
		console.log("Updating field: ")
		console.log(field);
		//This delete will force the entity to refresh the converted data
		//TODO: this means that if one element in the array is updated, all of them are grabbed again. Because they're cached, this isn't that bad--but if we wanted to update only the entities that were added/removed, that'd be cool too
		delete this._convertedData[field];
		this._data[field] = value;
		const fieldListeners = this._fieldListeners[field];
		//Right now, this undefined check is unnecessary. However, if we change how fieldlistener is implemented in the future, this will become necessary
		if(typeof fieldListeners === "undefined")return;
		for(let listenerCallback of fieldListeners){
			//We force the listener to get the value by awaiting the entity's field, in case it's an entity field.
			listenerCallback(this, field);
		}

	}

	equals(otherEntity){
		if(typeof otherEntity === "undefined")return false;
		return otherEntity._kind === this._kind && otherEntity._identifier === this._identifier;
	}

	setDeleted(){
		//This is really meant to be a one-way street. Deleted entities cannot be recovered
		console.log("Deleting, unloading from");
		console.log(this._swLoadedElements);
		for(let element of this._swLoadedElements){
			if(!this.equals(element._swLoadedEntity))return;
			SuggyUtils.clearElement(element);
			delete element._swLoadedEntity;
		}
		this._deleted = true;
	}
}

function base_main(){
	const sweb = new SuggyWeb();
}	

base_main();