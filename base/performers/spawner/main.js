/*
    This is our spawner script. This code is responsible for:
        - Starting up subgroups
        - Performing a heartbeat health check on subgroups
            - Updating our info file 
            - Making sure that at least one instance of the subset is always up
*/
module.exports = function(sutils) {
	const Performer = sutils.require("performer.js");
	const Logger = sutils.require("logger.js");

	class SpawnPerformer extends Performer {

		constructor(){
			super();
		}

		perform(){
			Logger.log("SpawnerPerformer", "Performing");
		}
	}

	return SpawnPerformer;

};

/*
var fs = require("fs");
var path = require("path");

const subsetsDirectory = "./subsets";
const subsets = [];

// Loop through all the files in the temp directory
fs.readdir(subsetsDirectory, function(err, files) {
    files.forEach(function(file) {
        const filePath = path.resolve(subsetsDirectory, file);
        fs.stat(filePath, function(err, fileStat) {
            if (fileStat.isDirectory()) {
                const infoPath = filePath + "/info.json";
                const subsetInfo = JSON.parse(fs.readFileSync(infoPath, "utf8"));
                subsets.push(subsetInfo);   
            }
        });
    });
});
*/