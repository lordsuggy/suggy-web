module.exports = function(sutils){
	//Developer should be able to call await entity.fieldName to get the value
	class SuggyEntity {
		constructor(kindInfo, entityData, db){
			this._kindInfo = kindInfo;
			this._data = entityData;
			this._surfaceData = {};
			this._db = db;
			this._kind = kindInfo.name;
			this._identifier = entityData._id;
			this._dirtyFields = [];
			this.init();

		}

		init() {
			const kindInfo = this._kindInfo;
			const thiz = this;
			//Go through the fields and create getters for them as appropriate
			for(let field of kindInfo.fields) {
				const fieldName = field.name;
				const fieldType = field.type;
				if(fieldType === "boolean" || fieldType === "number" || fieldType === "string"){
					thiz.setSimpleField(fieldName, fieldType);
				}else if(fieldType === "array"){
					thiz.setArrayField(fieldName);
				}else if(fieldType === "dictionary"){
					thiz.setDictionaryField(fieldName);
				}else if(fieldType === "entity"){
					thiz.setEntityField(fieldName);
				}else if(fieldType === "entity-array"){
					thiz.setEntityArrayField(fieldName);
				}else if(fieldType === "entity-dictionary"){
					thiz.setEntityDictionaryField(fieldName);
				}
			}
		}

		_addDirtyField(fieldName){
			if(this._dirtyFields.indexOf(fieldName) !== -1)return;
			this._dirtyFields.push(fieldName);
		}

		clearDirtyFields(){
			this._dirtyFields.length = 0;
		}

		get dirtyFields(){
			return this._dirtyFields;
		}

		setSimpleField(fieldName, fieldType, defaultValue) {
			const thiz = this;
			Object.defineProperty(thiz, fieldName, {
				get: async function () {
					var currentValue = thiz._surfaceData[fieldName];
					if(typeof currentValue === "undefined"){
						currentValue = this._data[fieldName];
						if(typeof currentValue === "undefined")currentValue = null;
						thiz._surfaceData[fieldName] = currentValue;
						//If it's still undefined, that's fine -- as long as we reflect what's in the data
					}
					return currentValue;
				}
				, set: function(val) {
					if(typeof val === fieldType){
						thiz._surfaceData[fieldName] = val;
					}
				}
			});
		}

		async setArrayField(fieldName) {
			const thiz = this;
			Object.defineProperty(thiz, fieldName, {
				get: async function () {
					var currentValue = thiz._surfaceData[fieldName];
					if(typeof currentValue === "undefined"){
						currentValue = thiz._data[fieldName];
						//If it's still undefined, we'll want to create a new array
						if(typeof currentValue === "undefined"){
							currentValue = [];
						}
						currentValue = sutils.copyArray(currentValue);
						thiz._surfaceData[fieldName] = currentValue;
					}
					return currentValue;
				}
				, set: function(val) {
					if(Array.isArray(val)){
						thiz._surfaceData[fieldName] = val;
					}
				}
			});
		}

		async setDictionaryField(fieldName) {
			const thiz = this;
			Object.defineProperty(thiz, fieldName, {
				get: async function () {
					var currentValue = thiz._surfaceData[fieldName];
					if(typeof currentValue === "undefined"){
						currentValue = thiz._data[fieldName];
						if(typeof currentValue === "undefined"){
							currentValue = {};
						}
						currentValue = sutils.copyObject(currentValue);
						thiz._surfaceData[fieldName] = currentValue;
					}
					return currentValue;
				}
				, set: function(val) {
					if(typeof val === "object" && !Array.isArray(val)){
						thiz._surfaceData[fieldName] = val;
					}
				}
			});
		}

		async setEntityField(fieldName) {
			const thiz = this;
			Object.defineProperty(thiz, fieldName, {
				get: async function () {
					var currentEntity = thiz._surfaceData[fieldName];
					if(typeof currentEntity !== "undefined") return currentEntity;

					//If it's undefined, we'll do our best to find it
					var currentValue = thiz._data[fieldName];

					//If it's undefined in the data, we can return null
					if(typeof currentValue === "undefined")return null;
					
					currentEntity = await thiz.getEntityFromValue(currentValue);
					thiz._surfaceData[fieldName] = currentEntity;
					
					return currentEntity;

				}
				, set: function(val) {
					if(!(val.constructor.name === SuggyEntity.name))return;
					//thiz._data[fieldName] = thiz.getValueFromEntity(val);
					thiz._surfaceData[fieldName] = val;
				}
			});
		}

		async setEntityArrayField(fieldName) {
			const thiz = this;
			Object.defineProperty(thiz, fieldName, {
				get: async function () {
					var currentValue = thiz._surfaceData[fieldName];
					//If it isn't undefined, we've already got the values
					if(typeof currentValue !== "undefined")return currentValue;

					//If it is undefined, we need to get the values
					currentValue = thiz._data[fieldName];
					
					//If the currentValue is undefined in the raw data, then return an empty array
					if(typeof currentValue === "undefined"){
						currentValue = [];
						this._surfaceData[fieldName] = currentValue;
						return currentValue;
					}

					//If the currentValue is defined in the raw data, pull it down
					const currentValuePromises = currentValue.map(function(val){
						return thiz.getEntityFromValue(val);
					});

					const convertedValue = await Promise.all(currentValuePromises);
					thiz._surfaceData[fieldName] = convertedValue;
					return convertedValue;
				}
				, set: function(vals) {

					if(!Array.isArray(vals))return;
					const unconvertedVals = vals.filter(function(val){
						return (val.constructor.name === SuggyEntity.name);
					}).map(function(val){
						return thiz.getValueFromEntity(val);
					});

					//thiz._data[fieldName] = unconvertedVals;
					thiz._surfaceData[fieldName] = vals;
				}
			});
		}

		async setEntityDictionaryField(fieldName) {
			const thiz = this;
			Object.defineProperty(thiz, fieldName, {
				get: async function () {
					var currentValue = thiz._surfaceData[fieldName];
					//If it isn't undefined, we've already got the values
					if(typeof currentValue !== "undefined")return currentValue;

					//If it is undefined, we need to get the values
					currentValue = thiz._data[fieldName];
					
					//If the currentValue is undefined in the raw data, then return an empty array
					if(typeof currentValue === "undefined"){
						currentValue = {};
						this._surfaceData[fieldName] = currentValue;
						return currentValue;
					}

					//If the currentValue is defined in the raw data, pull it down
					const currentValuePromises = Object.keys(currentValue).map(function(key){
						return thiz.getEntityFromValue(val).then(function(entity){
							currentValue[key] = entity;
						});
					});

					await Promise.all(currentValuePromises);
					thiz._surfaceData[fieldName] = currentValue;
					return currentValue;
				}
				, set: function(vals) {
					if(typeof vals !== "object" || Array.isArray(vals))return;

					const unconvertedVals = Object.keys(vals).filter(function(key){
						const val = vals[key];
						return (val.constructor.name === SuggyEntity.name);
					}).map(function(key){
						const val = vals[key];
						return thiz.getValueFromEntity(val);
					});


					//thiz._data[fieldName] = unconvertedVals;
					thiz._surfaceData[fieldName] = vals;
				}
			});
		}

		rawRemove(fieldName, entity){
			const val = this.getValueFromEntity(entity);
			const fieldValues = this._data[fieldName];
			if(!Array.isArray(fieldValues))return;

			const entityIndex = fieldValues.indexOf(val);
			if(entityIndex === -1)return;
			fieldValues.splice(entityIndex, 1);
			this._addDirtyField(fieldName);
			delete this._surfaceData[fieldName];
		}

		_unconvertEntityField(fieldName){
			const convertedEntity = this._surfaceData[fieldName];

			//TODO: figure out what happens to undefined fields, and how we can update them properly (set to null?)
			if(typeof convertedEntity === "undefined")return;
			const unconvertedEntity = this.getValueFromEntity(convertedEntity);

			if(unconvertedEntity === this._data[fieldName])return;
			this._addDirtyField(fieldName);
			this._data[fieldName] = unconvertedEntity;
		}

		_unconvertEntityArrayField(fieldName){
			const thiz = this;
			const convertedValue = this._surfaceData[fieldName];
			if(typeof convertedValue === "undefined")return;
			const unconvertedArray = convertedValue.map(function(entity){
				return thiz.getValueFromEntity(entity);
			});

			if(sutils.areArraysEqual(unconvertedArray, this._data[fieldName]))return;
			this._addDirtyField(fieldName);
			this._data[fieldName] = unconvertedArray;
		}

		_unconvertEntityDictionaryField(fieldName){
			const thiz = this;
			const convertedDictionary = this._surfaceData[fieldName];
			if(typeof convertedDictionary === "undefined")return;
			const unconvertedDictionary = {};
			for(var key in convertedDictionary){
				unconvertedDictionary[key] = this.getValueFromEntity(convertedDictionary[key]);
			}

			if(sutils.areObjectsEqual(unconvertedDictionary, this._data[fieldName]))return;
			this._addDirtyField(fieldName);
			this._data[fieldName] = unconvertedDictionary;
		}

		_unconvertGenericField(fieldName){
			const currentValue = this._surfaceData[fieldName];
			if(typeof currentValue === "undefined")return;//undefined means that nothing has changed; null may mean that the user deleted the value
			const dataValue = this._data[fieldName];
			if((currentValue === null && typeof dataValue === "undefined") || sutils.areEqual(currentValue, dataValue))return;
			
			this._addDirtyField(fieldName);
			this._data[fieldName] = sutils.copy(currentValue);
		}

		_unconvertData(){
			for(let field of this._kindInfo.fields) {
				const fieldName = field.name;
				const fieldType = field.type;
				if(fieldType === "entity"){
					this._unconvertEntityField(fieldName);
				}else if(fieldType === "entity-array"){
					this._unconvertEntityArrayField(fieldName);
				}else if(fieldType === "entity-dictionary"){
					this._unconvertEntityDictionaryField(fieldName);
				}else{
					this._unconvertGenericField(fieldName);
				}
			}
		}

		async getEntityFromValue(value){
			const valueSplit = value.split(":");
			const kind = valueSplit[0];
			const identifier = valueSplit[1];

			//TODO: should we cache this value if it's being synced?  -- Now being done
			return await this._db.read(kind, identifier);
		}

		getValueFromEntity(entity){
			return entity._kind + ":" + entity._identifier;
		}

		async update(){
			this._unconvertData();
			return await this._db.update(this);
		}

		async save(){
			await this.update();
		}

		async delete(){
			return await this._db.delete(this);
		}

		async addRole(userOrGroup, role){
			const roles = this._data._r;
			if(typeof userOrGroup !== "string"){
				userOrGroup = userOrGroup._identifier;
			}

			var userOrGroupRoles = roles[userOrGroup];
			if(typeof userOrGroupRoles === "undefined"){
				userOrGroupRoles = [];
				roles[userOrGroup] = userOrGroupRoles;
			}
			if(userOrGroupRoles.indexOf(role) !== -1) return;
			userOrGroupRoles.push(role);
		}

		async removeRole(userOrGroup, role){
			const roles = this._data._r;
			if(typeof userOrGroup !== "string"){
				userOrGroup = userOrGroup._identifier;
			}

			var userOrGroupRoles = roles[userOrGroup];
			if(typeof userOrGroupRoles === "undefined")return;
			var roleIndex = userOrGroupRoles.indexOf(role);
			if(roleIndex === -1) return;
			userOrGroupRoles.splice(roleIndex, 1);
		}

		userHasRole(user, role){
			const userId = user._identifier;
			const userRoles = this.getRolesForUser(user);
			return userRoles.indexOf(role) !== -1;
		}

		getRolesForUser(user){
			if(user === null)return [];
			const userId = user._identifier;
			const groupIds = user._data.groups;

			const roles = this._data._r;
			if(typeof roles === "undefined")return [];

			var allRoles = [];
			//Add user roles
			allRoles = allRoles.concat(roles[userId]);
			if(typeof groupIds !== "undefined"){
				//Add group roles
				for(let groupId of groupIds){
					allRoles = allRoles.concat(roles[groupId]);
				}
			}

			allRoles = sutils.uniquify(allRoles);
			return allRoles;
		}

		toJson(user){
			
			const finalData = {};
			const thiz = this;

			function copyData(name){
				finalData[name] = thiz._data[name];
			}

			const fields = this._kindInfo.fields;
			const userRoles = this.getRolesForUser(user);
			for(let field of fields){
				if(typeof field.hidden !== "undefined" && field.hidden)continue;
				const fieldName = field.name;
				const fieldRoles = field.roles;
				//TODO: check permissions here
				if(typeof fieldRoles === "undefined" || fieldRoles.length === 0){
					copyData(fieldName);
					continue;
				}

				//The field specifies a role, so make sure one of them matches a role we have
				var roleMatches = false;
				for(let userRole of userRoles){
					if(fieldRoles.indexOf(userRole) !== -1){
						roleMatches = true;
						break;
					}
				}

				if(roleMatches)copyData(fieldName);

				//Otherwise, this user doesn't have the proper permissions!
			}

			finalData._id = thiz._identifier;

			//const jsonString = JSON.stringify(finalData);
			//const jsonData = JSON.parse(jsonString)
			//delete jsonData._l;
			return {"kind":this._kind, "data":finalData};
		}

		equals(otherEntity){
			if(typeof otherEntity === "undefined")return false;
			return otherEntity._kind === this._kind && otherEntity._identifier === this._identifier;
		}
	}
	return SuggyEntity;
}