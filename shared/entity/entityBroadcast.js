//TODO: This is what function endpoints will reference to perform tasks like creations and updates
//All functions in this class will return a SuggyEntity
//

module.exports = function(sutils){

	const SuggyEntity = sutils.requireWithSutils("entity/entity.js");
	const Cache = sutils.require("database/cache.js");

	class EntityBroadcast {
		constructor(){
			this._cache = new Cache();
		}

		//Subscription methods
		getSubKey(kind, identifier){
			return kind+":"+identifier+":_l";
		}

		async filterListeners(kind, identifier) {
			//const listeners = await this.getListeners(kind, identifier);
			
			//const failedListeners = 
			//This will automatically filter any listeners that fail
			await this.notifyListeners(kind, identifier);
			/*
			console.log("Failed listeners in subscribe: ");
			console.log(failedListeners);
			for(let failedListener of failedListeners){
				const originIdIndex = listeners.indexOf(failedListener);
				if(originIdIndex === -1)continue;//This shouldn't be the case, but just in case...
				listeners.splice(originIdIndex, 1);
			}
			const key = this.getSubKey(kind, identifier);
			await this._cache.set(key, listeners);
			*/
		
		}

		async getListeners(kind, identifier){
			const key = this.getSubKey(kind, identifier);

			var listeners = await this._cache.get(key)
			return listeners;
			
		}
		async subscribe(kind, identifier, originId){
			const key = this.getSubKey(kind, identifier);
			console.log("Subscribing listener")
			console.log(originId);

			var listeners = await this.getListeners(kind, identifier);
			if(typeof listeners === "undefined"){
				listeners = [originId];
				await this._cache.set(key, listeners);
			}else {
				if(listeners.indexOf(originId) !== -1)return;

				//Perform a clean every 10 or so times, to make sure we clear out dead listeners
				if(Math.random() < 0.1){
					await this.filterListeners(kind, identifier);
				}
				listeners.push(originId);
			}
			
			//Update the cache because we may not be altering the reference
			await this._cache.set(key, listeners);

		}

		async unsubscribe(kind, identifier, originId){

			
			const listeners = await this.getListeners(kind, identifier);
			if(typeof listeners === "undefined")return;

			const originIdIndex = listeners.indexOf(originId);

			if(originIdIndex === -1)return;
			listeners.splice(originIdIndex, 1);
			const key = this.getSubKey(kind, identifier);
			await this._cache.set(key, listeners);
		}

		//Helper functions
		async notifyListeners(kind, identifier, message){

			const listeners = await this.getListeners(kind, identifier);
			console.log("Notifying listeners")
			console.log(listeners);
			if(typeof listeners === "undefined")return;

			console.log("Sending to listeners " + listeners);
			const failedListeners = await sutils.sendDataToOrigins(message, listeners);//We could await here, but we really don't care about the results--so long as it gets sent, we aren't waiting on a response
			
			for(let failedListener of failedListeners){
				const originIdIndex = listeners.indexOf(failedListener);
				if(originIdIndex === -1)continue;//This shouldn't be the case, but just in case...
				listeners.splice(originIdIndex, 1);
			}
			const key = this.getSubKey(kind, identifier);
			await this._cache.set(key, listeners);

		}

	}
	return EntityBroadcast;
}