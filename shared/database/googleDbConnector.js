const Datastore = require("@google-cloud/datastore");
const sizeof = require('object-sizeof');
const DbConnector = require("./dbConnector.js");
const SPLIT_THRESHOLD_IN_BYTES = 950;


class GoogleDbConnector extends DbConnector {
	
	constructor(sutils, externalProjectKeys){
		super();
		if(typeof externalProjectKeys === "undefined"){
			//const credentials = await sutils.readCredentials("gcp-service");
			this._datastore = Datastore({
				prefix: "sweb"

				// For convenience, @google-cloud/datastore automatically looks for the
				// GCLOUD_PROJECT environment variable. Or you can explicitly pass in a
				// project ID here:
				//, projectId: credentials.project_id

				// For convenience, @google-cloud/datastore automatically looks for the
				// GOOGLE_APPLICATION_CREDENTIALS environment variable. Or you can
				// explicitly pass in that path to your key file here:
				, keyFilename: sutils.getCredentialsPath("gcp-service")
			});
		} else {
			const projectId = externalProjectKeys.gcp.id;
			const projectKey = externalProjectKeys.gcp.key;

			const decodedProjectKey = sutils.decodeBase64(projectKey);
			const decodedProjectKeyData = JSON.parse(decodedProjectKey);

			const credentials = decodedProjectKeyData;
			console.log("External credentials:");
			console.log(credentials);


			this._datastore = Datastore({
				prefix: "sweb"

				// For convenience, @google-cloud/datastore automatically looks for the
				// GCLOUD_PROJECT environment variable. Or you can explicitly pass in a
				// project ID here:
				//, projectId: projectId
				, credentials: credentials
			});
		}
	}

	//TODO: be able to split >1mb entities?
	async create(kind, identifier, data){
		const datastore = this._datastore;

		const transaction = datastore.transaction();
		const key = datastore.key([kind, identifier]);

		const newData = this.splitData(data);

		return datastore.save({
			key: key,
			data: newData
		}).then(function(createdEntity){
			//The argument passed into this function is actually a list of mutations
			//console.log("Created " + kind);
			//console.log(data);
			data._id = identifier;
			return data;
		});
	}


	//TODO: be able to combine >1mb entities?
	async read(kind, identifier){
		const thiz = this;
		const datastore = this._datastore;

		const key = datastore.key([kind, identifier]);
		
		return datastore.get(key)
		.then(function(entityList){
			const entity = entityList[0];
			if(typeof entity === "undefined")throw "Entity not found"
			thiz.cleanResult(entity);
			thiz.combineData(entity);
			return entity;
		});
	}


	//TODO: be able to split >1mb entities?
	async update(kind, identifier, data){
		const thiz = this;
		const datastore = this._datastore;
		const transaction = datastore.transaction();

		const splitData = thiz.splitData(data);

		const key = datastore.key([kind, identifier]);
		var entity;
		return datastore.save({
			key: key,
			data: splitData
		})
		.then(function(savedData){
			data._id = identifier;
			return data;
		}).catch(function(err){
			console.log("-----Error in save:");
			console.log(data);
			console.log(err);
			throw err;
		});

	}

	async delete(kind, identifier){
		const thiz = this;
		const datastore = this._datastore;
		var data;

		const key = datastore.key([kind, identifier]);

		//Start the transaction
		return datastore.delete(key).then(function(){
			return data;
		}).catch(function(err){
			throw err;
		});
	}

	async query(kind, filters){
		const thiz = this;
		var query = this._datastore.createQuery(kind);
		for(let filter of filters){
			//TODO: check what type of equality check is used in the filter.
			query = query.filter(filter.key, "=", filter.value);
		}

		const results = await this._datastore.runQuery(query);	
		return results[0].map(function(result){
			thiz.cleanResult(result);
			thiz.combineData(result);
			return result;
		});
	}

	async querySingle(kind, filters){
		const results = await this.query(kind, filters);
		if(results.length > 1) throw "Found too many results";
		if(results.length === 0) throw "Found no results";
		const result = results[0];
		thiz.cleanResult(result);
		thiz.combineData(result);
		return result;
	}

	cleanResult(result){
		//console.log(result);
		result._id = result[Datastore.KEY].name;
		delete result[Datastore.KEY];
		
	}

	splitData(data){
		const newData = {};
		//We'll go through each of its members and if it's greater than 1kb (950 bytes, for safety in overhead)
		//then split it and create a new field
		for(var key in data){
			var value = data[key];
			var currentSize = sizeof(value);
			if(currentSize <= SPLIT_THRESHOLD_IN_BYTES){
				newData[key] = value;
			}else {
				const valueType = typeof(value);
				var splitCount = 0;
				var previousSize = -1;
				var newKey;
				var done = false;
				while(!done){
					newKey = key + "_s" + splitCount;
					var miniValue;
					if(valueType === "string"){
						miniValue = value.substring(0, SPLIT_THRESHOLD_IN_BYTES);
						value = value.substring(SPLIT_THRESHOLD_IN_BYTES);
						

					}else if(Array.isArray(value)){
						miniValue = [];
						while(sizeof(miniValue) < SPLIT_THRESHOLD_IN_BYTES && value.length != 0){
							miniValue.push(value.shift());
						}

						//This was the value that made it get too big, so put it back on the value
						value.unshift(miniValue.pop());
						console.log("SPLITTING ARRAY");
						console.log(value);
						console.log(miniValue);

					}

					//TODO: split dictionaries as well
					/*else if(valueType === "object"){

					}*///TODO: There shouldn't be an else, but if there is, we could try assuming it's just some yummy bytes.
					if(splitCount == 0){
						newData[key] = miniValue;
					}else {
						newData[newKey] = miniValue;
					}

					previousSize = currentSize;
					currentSize = sizeof(value);
					splitCount += 1;

					if(currentSize <= SPLIT_THRESHOLD_IN_BYTES || currentSize == 0) {
						done = true;
					}
				}
				console.log("Final split count: " + splitCount);
				//At the end, set the last of the value
				newKey = key + "_s" + splitCount;
				newData[newKey] = value;
				console.log(newData);
			}

		}
		

		return newData;
	}

	combineData(data){
		console.log("Combining data");
		for(var key in data){
			console.log(key);
			var value = data[key];
			const valueType = typeof(value);
			var splitCount = 1;//We start at 1 because 0 is the key au natural
			var splitFound = true;
			while(splitFound){
				const nextKey = key + "_s" + splitCount;
				const nextValue = data[nextKey];
				splitFound = typeof nextValue !== "undefined";
				if(!splitFound)break;

				if(valueType === "string"){
					data[key] += nextValue;
					
				}else if(Array.isArray(value)){
					data[key] = data[key].concat(nextValue);
				}

				delete data[nextKey];
				splitCount += 1;
			}

		}
	}
}

module.exports = GoogleDbConnector;