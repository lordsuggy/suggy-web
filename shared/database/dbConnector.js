

class DbConnector {

	async create(kind, identifier, data){

	}

	async read(kind, identifier){

	}

	async readBatch(kindIdentifiers){

	}

	async update(kind, identifier, data){

	}

	async delete(kind, identifier){

	}

	async query(kind, queries){

	}
	
	async querySingle(kind, filters){

	}

}

module.exports = DbConnector;