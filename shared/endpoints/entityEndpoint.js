module.exports = function(sutils){
	const Endpoint = require("./endpoint.js");

	const EntityDatabase = sutils.requireWithSutils("entity/entityDatabase.js")
	const EntityBroadcast = sutils.requireWithSutils("entity/entityBroadcast.js")

	class EntityEndpoint extends Endpoint {

		constructor() {

			super();
			this._db = new EntityDatabase();
			this._broadcast = new EntityBroadcast();
			this.usesUser = true;
		}

		_updateConfig(){
			super._updateConfig();
			this.resource = "e/"+this.config.resource;
		}
	
		//Our main functions for read, subscribe, unsubscribe, and kind info
		async readFunction(data, originId, user){
			const entityId = data.id;
			
			const returnData = await this._db.read(this.config.kind, entityId);
			//This class performs the check on whether or not the user can read
			//Update: now this is done by the entity's toJson method because it's on a per-field basis
			//if(!this._db.userCanRead(user, returnData._data))throw "User cannot read " + this.config.kind + ":" + entityId;

			const autoSubscribe = data.subscribe;
			if(typeof autoSubscribe !== "undefined" && autoSubscribe){
				await this._broadcast.subscribe(this.config.kind, data.id, originId);
			}
			return returnData;
		}

		async subscribeFunction(data, originId, user){
			console.log("Subscribing");
			await this._broadcast.subscribe(this.config.kind, data.id, originId);
		}

		async unsubscribeFunction(data, originId, user){
			await this._broadcast.unsubscribe(this.config.kind, data.id, originId);
		}

		async kindInfoFunction(data, originId, user){
			//TODO: filter fields based on the user
			const fields = [];
			for(let field of this.config.fields){
				if(field.hidden)continue;
				fields.push(field);
			}
			const returnInfo = {"name":this.config.kind, "fields":fields};
			if(typeof this.config.meta !== "undefined"){
				returnInfo.meta = this.config.meta;
			}
			return returnInfo;
		}

		async process(data, originId, user, updateProgress){
			const action = data.action;
			//const properVerb = EntityEndpoint.ACTION_TO_VERB[action];
			//if(properVerb !== verb) throw `Incorrect verb (${verb}) for action ${action}`;
		
			const functionName = EntityEndpoint.ACTION_TO_FUNCTIONS[action];
			var returnData;
			const funcName = functionName;

			//The functions should be names like createFunction and readFunction
			const fullFuncName = `${funcName}Function`;
			
			returnData = await this[fullFuncName](data.inputs, originId, user, updateProgress);
			
			return returnData;
		}


	}

	EntityEndpoint.ACTIONS = Object.freeze({
		//"CREATE":"c"
		"READ":"r"
		//, "READ_OR_CREATE":"e"
		//, "UPDATE_OR_CREATE":"t"
		//, "UPDATE":"u"
		//, "PATCH":"p"
		//, "DELETE":"d"

		, "SUBSCRIBE":"s"
		, "UNSUBSCRIBE":"b"

		, "KIND_INFO":"k"
	});

	EntityEndpoint.ACTION_TO_VERB = Object.freeze({
		//"c":"POST"
		//, "e":"POST"
		 "s":"POST"
		, "b":"POST"
		, "r":"GET"
		//, "t":"PUT"
		//, "u":"PUT"
		//, "p":"PATCH"
		//, "d":"DELETE"
		, "k": "GET"
	});

	EntityEndpoint.ACTION_TO_FUNCTIONS = Object.freeze({
		//"c":["create"]
		//, "e":["read", "create"]//TODO: does this really get used?
		"r":"read"
		//, "t":["update", "create"]//TODO: this shoudl really just be upsert
		//, "u":["update"]
		//, "p":["patch"]
		//, "d":["delete"]
		, "s":"subscribe"
		, "b":"unsubscribe"
		, "k":"kindInfo"
	});

	return EntityEndpoint;
}
