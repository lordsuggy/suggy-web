class Variable {

	constructor(name, type) {
		this.name = name;
		this.type = type;
	}


	get name(){
		return this._name;
	}

	set name(val){
		this._name = val;
	}


	get type(){
		return this._type;
	}

	set type(val){
		this._type = val;
	}

}

Variable.TYPES = Object.freeze({"BOOLEAN":"boolean", "NUMBER":"number", "STRING":"string", "ARRAY":"array", "DICTIONARY":"dictionary", "ENTITY":"entity", "ENTITY_ARRAY":"entity-array", "ENTITY_DICTIONARY":"entity-dictionary", "DYNAMIC":"dynamic"});


module.exports = Variable;