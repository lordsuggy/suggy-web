const Variable = require("./variable.js");

class OutputVariable extends Variable{

	constructor(name, type) {
		super(name, type);
	}

}


module.exports = OutputVariable;