const Variable = require("./variable.js");

class InputVariable extends Variable{

	constructor(name, type, isRequired = false) {
		super(name, type);
		this.isRequired = isRequired;
	}

	get isRequired(){
		return this._isRequired;
	}

	set isRequired(val){
		this._isRequired = val;
	}

}


module.exports = InputVariable;