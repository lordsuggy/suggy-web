class Provider {

	get name(){ return "Provider"; }

	constructor(sutils) {
		this.sutils = sutils;
	}	

}

Provider.PROVIDERS = Object.freeze({"AWS":"aws", "GCP":"gcp"});

Provider.MACHINE_SIZES = Object.freeze({"MICRO":"machine-size-micro", "TINY":"machine-size-tiny", "SMALL":"machine-size-small", "MEDIUM":"machine-size-medium", "LARGE":"machine-size-large", "HUGE":"machine-size-huge"});

Provider.ZONES = Object.freeze({"US_EAST":"us-east", "US_CENTRAL":"us-central", "US_WEST":"us-west"});

Provider.Create = function(providerName, ...args){
	const providerPath = "./"+providerName+"/provider.js";
	const ProviderClass = require(providerPath);
	return new ProviderClass(...args);
};

module.exports = Provider;